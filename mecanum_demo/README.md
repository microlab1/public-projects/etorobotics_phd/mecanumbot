# mecanum_demo

This package contains a demo application for following the owner with camera movement and setting the 
color of the LED based on the distace information

## Bringup

    [PC-Terminal]: $ roscore
    [PI-Terminal]: $ roslaunch mecanumbot_core mecanumbot_robot.launch
    [PI-Terminal]: $ rosrun mocapstream stream_publisher.py
    [PC-Terminal]: $ roslaunch mecanumbot_core mecanum_control.launch
    [PC-Terminal]: $ roslaunch mecanum_demo demo.launch

## TODO 

 - [ ] Change camera control from service to topic
 - [ ] Grabber initialization not set
 - [ ] Angles are incorrect (sometimes the camera look to the wrong direction)
