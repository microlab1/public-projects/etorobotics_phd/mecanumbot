#!/usr/bin/env python
from typing import Match
import rospy
from mecanum_led.srv import ControlLed
import mecanum_led.msg
from mecanum_grabber.srv import ControlGrabber
from mecanum_grabber.srv import GetGrabberState
from mecanum_neck.srv import ControlNeck
from mocapstream.msg import *
from geometry_msgs.msg import Twist, Vector3
import math
import numpy as np

tracked = "OWN"
robot = "ROB"

tolerance = 1

posx = 0
posy = 0
posz = 0

robx = 0
roby = 0
robz = 0
rob_yaw = 0

eps_x = 0.1
eps_y = 0.07
eps_z = 0

def get_neck_angle(distance, vec_rob, vec_own, rob_angle):
    global rob_yaw
    y_dist = (vec_rob[1] - vec_own[1])
    d = ((robx + eps_x - posx) ** 2 + (robz + eps_z - posz) ** 2) ** 0.5
    if d == 0:
        delta = 0
    else:
        delta = math.tan(y_dist / d)
    
    e_rob = [math.cos(rob_yaw*math.pi/180), -1 * math.sin(rob_yaw*math.pi/180) ]
    e_own = [(vec_own[0] - vec_rob[0]) , (vec_own[2] - vec_rob[2])]
    e_own = e_own / np.linalg.norm(e_own)

    dot_product = np.dot(e_own, e_rob)
    alpha = np.arccos(dot_product)

    if alpha > 90:
        alpha = 90

    if e_own[1] > 0:
        alpha *= -1

    if delta > 90:
        delta = 90

    if delta < -90:
        delta = -90


    alpha *= (180 /math.pi)
    print("alpha: " + str(alpha))
    delta *= (180 /math.pi)
    return (delta, alpha)





def stream_callback(msg):
    global posx, posy, posz, robx, roby, robz, rob_yaw
    for body in msg.data:
        if body.name == tracked:
            posx = body.pos.x
            posy = body.pos.y
            posz = body.pos.z
        if body.name == robot:
            robx = body.pos.x
            roby = body.pos.y
            robz = body.pos.z
            rob_yaw = body.orientation.euler.yaw


def move():
    d = ((robx - posx) ** 2 + (robz - posz) ** 2) ** 0.5
    neck_client = rospy.ServiceProxy("control_neck", ControlNeck)

    delta, alpha = get_neck_angle(d, [robx + eps_x, roby + eps_y, robz + eps_z], [posx, posy, posz], rob_yaw)
    
    print(alpha)
    neck_client(delta, alpha)
    print("done")





def main():
    rospy.init_node("mecanum_demo_neck")

    rospy.wait_for_service("control_neck")

    rospy.Subscriber("stream", StreamVector, stream_callback)

    rospy.loginfo("demo node started")
 
    rate = rospy.Rate(40)

    while not rospy.is_shutdown():
        move()
        rate.sleep()





if __name__ == "__main__":
    main()