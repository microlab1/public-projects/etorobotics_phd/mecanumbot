#!/usr/bin/env python
from typing import Match
import rospy
from mecanum_led.srv import ControlLed
import mecanum_led.msg
from mecanum_grabber.srv import ControlGrabber
from mecanum_grabber.srv import GetGrabberState
from mecanum_neck.srv import ControlNeck
from mocapstream.msg import *
from geometry_msgs.msg import Twist, Vector3
import math
import numpy as np

tracked = "OWN"
robot = "ROB"

tolerance = 1

posx = 0
posy = 0
posz = 0

robx = 0
roby = 0
robz = 0

def stream_callback(msg):
    global posx, posy, posz, robx, roby, robz
    for body in msg.data:
        if body.name == tracked:
            posx = body.pos.x
            posy = body.pos.y
            posz = body.pos.z
        if body.name == robot:
            robx = body.pos.x
            roby = body.pos.y
            robz = body.pos.z

def move():
    d = ((robx - posx) ** 2 + (robz - posz) ** 2) ** 0.5
    if d == 0:
        d = 5
    
    if d > 0.5:
        led_client = rospy.ServiceProxy("control_led", ControlLed)
        color_1 = mecanum_led.msg.Color(0, 0, 50)
        color_2 = mecanum_led.msg.Color(0, 0, 0)

        colors = [color_1, color_2]

        req = mecanum_led.srv.ControlLedRequest(colors, 1 / d * 3, False, "blink")
    else:
        led_client = rospy.ServiceProxy("control_led", ControlLed)
        color_1 = mecanum_led.msg.Color(0, 50, 0)

        colors = [color_1]

        req = mecanum_led.srv.ControlLedRequest(colors, 1 / d * 3, False, "blink")

    led_client(req)

def main():
    rospy.init_node("mecanum_demo_led")

    rospy.wait_for_service("control_led")

    rospy.Subscriber("stream", StreamVector, stream_callback)

    rospy.loginfo("demo node started")
 
    rate = rospy.Rate(1)

    while not rospy.is_shutdown():
        move()
        rate.sleep()





if __name__ == "__main__":
    main()