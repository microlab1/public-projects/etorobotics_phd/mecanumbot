# MecanumBot

This repository is made to control a MecanumBot. The [mecanumbot_core](https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot_core) 
is the target robot repository connected to this project. Different ros packages are included to 
implement a variety of functionalities. To fully use the functionalities of this project the [mocapstream](https://gitlab.com/microlab1/private-projects/etorobotics/mocapros)
provide the connection with an external motion capture system via wifi stream. <br>

Tested with: <br> 
**Ubuntu 20.04** <br>
**ROS 1 - Noetic** <br>

This project took the Turtlebot3 robot as inspiration and base. The original documentation of the Turtlebot3 robots can be
seen at [Robotis eManual](https://emanual.robotis.com/docs/en/platform/turtlebot3/overview/#overview)

To make it easier to follow the description and usage below:
- **[PC-Terminal]** is used to refer to the Master PC running the `roscore`.
- **[PI-Terminal]** is used to refer to the onboard Raspberry Pi on the robot.

## Table of Contents
[[_TOC_]]

## Dependencies
The project mainly Python 3 based.

### Low level packages
These packages are necessary to run the project.

| ROS package                                                                                  | Description                                                                                              |
|----------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------|
| [mocapstream](https://gitlab.com/microlab1/private-projects/etorobotics/mocapros)            | The Mocap stream provide the position of the tracked agents in the observed space.                       |
| [mecanumbot_core](https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot_core) | The mecanumbot_core package provide the functionalities need to be built on the on board Raspberry PI    |
|[turtlebot3](https://github.com/ROBOTIS-GIT/turtlebot3) | The turtlebot3 packege provide the legacy turtlebot3 code integrated into this project such as the LIDAR |
|[turtlebot3_msgs](https://github.com/ROBOTIS-GIT/turtlebot3_msgs) | The turtlebot3_msgs package provide the standard messages used in the turtlebot projects.                |

### High level packages 
These packages use this repository as dependency and implement higher level functionalities

| ROS package            | Description                         |
|------------------------|-------------------------------------|
| [ros_behaviourmodel](https://gitlab.com/microlab1/private-projects/etorobotics/behaviourmodel) | Base behaviour model for mecanumbot |

## Setup

### On the Master PC (referenced as **[PC-Terminal]**)

    [PC-Terminal]: $ cd ~/catkin_ws/src
    [PC-Terminal]: $ git clone https://gitlab.com/microlab1/private-projects/etorobotics/mocapros.git
    [PC-Terminal]: $ git clone https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot.git
    [PC-Terminal]: $ sudo apt install ros-noetic-turtlebot3-msgs
    [PC-Terminal]: $ sudo apt install ros-noetic-turtlebot3
    [PC-Terminal]: $ cd ~/catkin_ws
    [PC-Terminal]: $ catkin_make

Optional:

    [PC-Terminal]: $ cd ~/catkin_ws/src
    [PC-Terminal]: $ git clone https://gitlab.com/microlab1/private-projects/etorobotics/behaviourmodel.git
    [PC-Terminal]: $ cd ~/catkin_ws
    [PC-Terminal]: $ catkin_make

### On the MecanumBot (referenced as **[PI-Terminal]**)

    [PI-Terminal]: $ cd ~/catkin_ws/src
    [PI-Terminal]: $ git clone https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot_core.git
    [PI-Terminal]: $ sudo apt install ros-noetic-turtlebot3-msgs
    [PI-Terminal]: $ sudo apt install ros-noetic-turtlebot3
    [PI-Terminal]: $ cd ~/catkin_ws
    [PI-Terminal]: $ catkin_make

After the successful build the project is ready to use.

## Package functionalities

| **Package**        | **Functionalities**                                                      |
|--------------------|--------------------------------------------------------------------------|
| [mecanum](https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot/-/tree/main/mecanum)        | Main package containing the launch files                                 |
| [mecanum_demo](https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot/-/tree/main/mecanum_demo)     | Demo application to present the camera tracking ability and led blinking |
| [mecanum_grabber](https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot/-/tree/main/mecanum_grabber)  | Package to manage the grabber control tasks                              |
| [mecanum_led](https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot/-/tree/main/mecanum_led)      | Package to manage the LED control tasks                                  |
| [mecanum_neck](https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot/-/tree/main/mecanum_neck)     | Package to manage the neck mechanism control tasks                       |
| [mecanum_teleop](https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot/-/tree/main/mecanum_teleop) | Package to provide a keyboard control for the MecanumBot                 |

## Bringup

Mecanumbot IP and Master PC IP should be checked before every bringup. <br>
The following .bashrc scripts can help, to set the proper IP addresses connected to known wifi networks. 

<details>
<summary>Automatic IP setting from .bashrc</summary>
<br>
.bashrc file update for Raspberry

    export ROS_HOSTNAME=$(hostname -I | sed 's/ *$//g')

    if [[ "$ROS_HOSTNAME" = "10.0.0.12" ]]; then
        export ROS_MASTER_URI=http://10.0.0.113:11311
    elif [[ "$ROS_HOSTNAME" = "192.168.100.111" ]]; then
        export ROS_MASTER_URI=http://192.168.100.156:11311
    fi
    
    echo ROS_MASTER_URI=$ROS_MASTER_URI
    echo ROS_HOSTNAME=$ROS_HOSTNAME

.bashrc file update for Master PC
    
    export ROS_MASTER_URI=http://$(hostname -I | sed 's/ *$//g'):11311
    export ROS_IP=$(hostname -I | sed 's/ *$//g')

    echo ROS_MASTER_URI=$ROS_MASTER_URI
    echo ROS_IP=$ROS_IP
</details>

Fix mecanum Raspberry IPs on different wifi networks:<br>
IP: **10.0.0.12** (D411) <br>
IP: **192.168.1.103** (D422) <br>
IP: **192.168.100.111** (D510) <br>

### Basic 
Use the proper IP to ssh into the mounted Raspberry PI and make a **PI - Terminal**:

      $ ssh ubuntu@10.0.0.12

And Log in. <br>
Login: **ubuntu** <br>
password: **turtlebot** <br>

Bring up the robot

    [PC-Terminal]: $ roscore
    [PI-Terminal]: $ roslaunch mecanumbot_core mecanumbot_robot.launch

To control the robot:

    [PC-Terminal]: $ rosrun mecanum_teleop mecanum_teleop_key
    
To test Feedback of robot voltage, LED, Grabber and Neck functionalities:

    [PC-Terminal]: $ rostopic echo /rob_voltage 
    [PC-Terminal]: $ rostopic pub -1 /cmd_led geometry_msgs/Vector3 -- '10.0' '0.0' '0.0'
    [PC-Terminal]: $ rostopic pub -1 /cmd_grabber geometry_msgs/Vector3 -- '623.0' '400.0' '0.0'
    [PC-Terminal]: $ rostopic pub -1 /cmd_neck geometry_msgs/Vector3 -- '2000.0' '1000.0' '0.0'


To set the Grabber, Neck and LED controller nodes:

    [PC-Terminal]: $ roslaunch mecanumbot_core mecanum_control.launch

### Camera track demo
ROB and OWN should be defined in the Motion Capture stream before launch this application. Before starting the `stream_publisher.py` check the following in the 

Motive software: <br>
The Wifi is connected to the **Mozgaslabor** WiFi <br>
Streaming pane -> Local Interface: **192.168.100.105** <br> 
Streaming pane -> Up Axis: **Y UP**


    [PC-Terminal]: $ roscore

    [PI-Terminal]: $ roslaunch mecanumbot_core mecanumbot_robot.launch

    [PC-Terminal]: $ rosrun mocapstream stream_publisher.py
    [PC-Terminal]: $ roslaunch mecanumbot_core mecanum_control.launch
    [PC-Terminal]: $ roslaunch mecanum_demo demo.launch

### Bring TOY to OWN
ROB, TOY and OWN should be defined in the Motion Capture stream before launch this application.

    [PC-Terminal]: $ roscore

    [PI-Terminal]: $ roslaunch mecanumbot_core mecanumbot_robot.launch

    [PC-Terminal]: $ rosrun mocapstream stream_publisher.py
    [PC-Terminal]: $ roslaunch mecanumbot_core mecanum_control.launch
    [PC-Terminal]: $ rosrun ros_behaviourmodel start_behaviourmodel.py

## Authors and acknowledgment
Balázs Nagy (email: nagybalazs@mogi.bme.hu) <br>
Kristóf Botond Bocsi <br>
Bálint Hegedűs <br>

## License
The project is open source under the license of BSD.

## References
[Seunghooon's repository](https://github.com/Seunghooon/turtlebot3_mecanum) <br>
[ROBOTIS-GIT](https://github.com/ROBOTIS-GIT/OpenCR/tree/master/arduino/opencr_arduino/opencr/libraries/turtlebot3/include/turtlebot3) <br>

## ToDo

- mecanum pkg
  - [ ] Collect the mid level .launch files.
- mecanum_demo
  - [ ] Tracking sometimes not working properly -> change camera control from service to topic.
- mecanum_grabber
  - [ ] Init position generate error, open the grabber motors on init launch.
- mecanum_led
  - [ ] Smooth change between different stages.
- mecanum_neck
  - [ ] Change the communication to topic.
- mecanum_teleop
  - [x] Implement mecanum drive (without external dependencies).
- mecanum_core
  - [ ] Implement ddometry (rob_odom) on OpenCR firmware.
- mocapstream
  - [ ] Check the Roll, Pitch, Yaw angles. Sometimes not precise.
- General
  - [x] Link the sub packages in the main readme.
  - [ ] Write single use cmd commands to test the functionalities of the sub packages (in the readme of the subpackage).
  - [ ] Make a gui package to monitor the robot.
  - [ ] Implement complex behaviours.
    - [ ] Attention node (focus can be set).
    - [ ] Follower node (target can e set).
  - [ ] Collect Python dependencies.
