# mecanum 

This package is the main package of the MecanumBot project. To use the full functionality
of the project use together with the MocapROS project.

## Dependencies

[mocapstream](https://gitlab.com/microlab1/private-projects/etorobotics/mocapros) 
- The Mocap stream provide the position of the tracked agents in the observed space. <br>

[mecanumbot_core](https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot_core)
- The mecanumbot_core package provide the functionalities need to be built on the on board Raspberry PI <br>

[turtlebot3](https://github.com/ROBOTIS-GIT/turtlebot3) 
- The turtlebot3 packege provide the legacy turtlebot3 code integrated into this project such as the LIDAR<br>

[turtlebot3_msgs](https://github.com/ROBOTIS-GIT/turtlebot3_msgs)
- The turtlebot3_msgs package provide the standard messages used in the turtlebot projects. <br>

## Setup

The repositories of the dependent packages contains the necessary setup information and more detailed
explanation about the functionalities implemented in the specific package. To install and build the before mentioned
packages use the following instructions:

On the Master PC (referenced as **[PC-Terminal]**)

    [PC-Terminal]: $ cd ~/catkin_ws/src
    [PC-Terminal]: $ git clone https://gitlab.com/microlab1/private-projects/etorobotics/mocapros.git
    [PC-Terminal]: $ git clone https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot.git
    [PC-Terminal]: $ sudo apt install ros-noetic-turtlebot3-msgs
    [PC-Terminal]: $ sudo apt install ros-noetic-turtlebot3
    [PC-Terminal]: $ cd ~/catkin_ws
    [PC-Terminal]: $ catkin_make

On the MecanumBot (referenced as **[PI-Terminal]**)

    [PI-Terminal] $ cd ~/catkin_ws/src
    [PI-Terminal] $ git clone https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot_core.git
    [PI-Terminal]: $ sudo apt install ros-noetic-turtlebot3-msgs
    [PI-Terminal]: $ sudo apt install ros-noetic-turtlebot3
    [PI-Terminal] $ cd ~/catkin_ws
    [PI-Terminal] $ catkin_make

After the successful build the project is ready to use.

## Bringup


## ToDo