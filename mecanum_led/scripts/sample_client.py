#!/usr/bin/env python
import rospy
from mecanum_led.srv import ControlLed
import mecanum_led.msg
from std_msgs.msg import String

# A simple client showing how to use the ControlLed service

def main():
    rospy.init_node("sample_client")
    rospy.wait_for_service("control_led")

    client = rospy.ServiceProxy("control_led", ControlLed)

    color_1 = mecanum_led.msg.Color(255, 0, 0)
    color_2 = mecanum_led.msg.Color(0, 255, 0)
    color_3 = mecanum_led.msg.Color(0, 0, 255)

    colors = [color_1, color_2, color_3]

    req = mecanum_led.srv.ControlLedRequest(colors, 1, True, "blink")


    client(req)



if __name__ == "__main__":
    main()
