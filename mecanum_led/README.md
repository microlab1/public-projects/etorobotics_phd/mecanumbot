# Mecanum Led

## Usage:
To start the node run:

        $ rosrun mecanum_led led_control

The node communicates on the `control_led` topic. To control the LEDs use the **ControlLed** service.

A new task automatically stops the previous one.

## Parameters:
- **Colors**: An arbitrary number of colors can be defined using RGB values. The color msg is defined in this package.
- **frequency**: The frequency of color change given in Hz (number of colors/sec).
- **gap**: (only in blink mode) if true the LEDs turn of for some time between every color.
- **mode**: ("blink" or "fade") if blink is set colors change instantly, if fade is set, color change is continuous

---

- **success**: true if the new task was started succesfully

## Options
A single color can be set by turning on blink mode and setting gap to false (in this case frequency is not used but you should still define it as some arbitrary value)

To turn a single color on and off repeatedly set a single color as the Color argument, turn on gap and blink mode and give the desired frequency.

To turn the LEDs off call the service with only one color with RGB values set to 0.

## Example
The sample_client.py node is an example client showing how to use the service.

        $ rosrun mecanum_led sample_client.py

