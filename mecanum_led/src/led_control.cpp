#include <ros/ros.h>

#include <mecanum_led/Color.h>
#include <mecanum_led/ControlLed.h>
#include <geometry_msgs/Vector3.h>
#include <std_msgs/String.h>

#include <thread>

class LedControl{
public:
    LedControl()
    {
        server = n.advertiseService("control_led", &LedControl::serviceCb, this);
        pub = n.advertise<geometry_msgs::Vector3>("cmd_led", 50);
        priority_thread = 0;
    }
private:
    bool serviceCb(mecanum_led::ControlLedRequest& req, mecanum_led::ControlLedResponse& res)
    {
        priority_thread++;;
        if(req.mode == "blink")
        {
            if(req.gap)
            {
                mecanum_led::Color empty;
                empty.red = 0;
                empty.green = 0;
                empty.blue = 0;
                std::vector<mecanum_led::Color> colors;
                for(std::vector<mecanum_led::Color>::iterator it = req.colors.begin(); it != req.colors.end(); ++it)
                {
                    colors.push_back(*it);
                    colors.push_back(empty);
                }
                std::thread blink(&LedControl::blink, this, colors, req.frequency, req.gap, priority_thread);
                blink.detach();
            }
            else
            {
                std::thread blink(&LedControl::blink, this, req.colors, req.frequency, req.gap, priority_thread);
                blink.detach();
            }
        }
        else
        {
            if(req.mode == "fade")
            {
                std::thread fade(&LedControl::fade, this, req.colors, req.frequency, priority_thread);
                fade.detach();
            }
            else
            {
                ROS_ERROR_STREAM("Invalid mode set in ControlLed service");
                res.success = false;
                return false;
            }
        }
        res.success = true;
        return true;
    }

    void fade(std::vector<mecanum_led::Color> colors, float frequency, unsigned long long int thread_num)
    {
        #define FADE_RATE 30
        ros::Rate loop_rate(FADE_RATE);
        float steps = FADE_RATE / frequency;
        while(thread_num == priority_thread)
        {
            for(std::vector<mecanum_led::Color>::iterator it = colors.begin(); it != colors.end(); ++it)
            {
                std::array<double, 3> distance;
                if(it != (colors.end() - 1))
                {
                    distance[0] = std::next(it, 1)->red - it->red;
                    distance[1] = std::next(it, 1)->green - it->green;
                    distance[2] = std::next(it, 1)->blue - it->blue;
                }
                else
                {
                    distance[0] = colors.begin()->red - it->red;
                    distance[1] = colors.begin()->green - it->green;
                    distance[2] = colors.begin()->blue - it->blue;
                }

                geometry_msgs::Vector3 msg;
                msg.x = it->red;
                msg.y = it->green;
                msg.z = it->blue;

                pub.publish(msg);

                loop_rate.sleep();
                for(int i = 0; i < steps; i++)
                {
                    if(thread_num != priority_thread)
                    {
                        return;
                    }
                    
                    msg.x += distance[0] / steps;
                    msg.y += distance[1] / steps;
                    msg.z += distance[2] / steps;

                    pub.publish(msg);

                    loop_rate.sleep();
                } //for i = 0
            } //for iterator
        } //while true
    } //fade()

    void blink(std::vector<mecanum_led::Color> colors, float frequency, bool gap, unsigned long long int thread_num)
    {
        ros::Rate loop_rate(frequency);
        while(thread_num == priority_thread)
        {
            for(std::vector<mecanum_led::Color>::iterator it = colors.begin(); it != colors.end(); ++it)
            {
                geometry_msgs::Vector3 msg;
                msg.x = it->red;
                msg.y = it->green;
                msg.z = it->blue;

                pub.publish(msg);

                loop_rate.sleep();
            } //for iterator
        } //while true
    } //blink()

    ros::NodeHandle n;
    ros::Publisher pub;
    ros::ServiceServer server;
    unsigned long long int priority_thread;
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "led_control");

    LedControl led_control;

    ROS_INFO_STREAM("ControlLed service ready");
    ros::spin();

    ros::shutdown();
    return 0;
}