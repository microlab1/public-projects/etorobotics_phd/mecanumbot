# Mecanum Grabber
This package provides a simple sevice to open and close the grabber on the mecanum bot.

## Usage 
To use the package run the main node with:

        $ rosrun mecanum_grabber grabber_contorl

After that you can call the ControlGrabber service on the `control_grabber` topic.
The service has only one argument. If it's set to `True` the grabber will close, if it's set to `False` the grabber will open.
The service returns `True` if the grabber reached the goal position.
On startup the control node opens the grabber.

The motor positions corresponding to the open and closed states are defined in the grabber_close.yaml file.

To read the current state of the grabber call the `GetGrabberState` service on the `"get_grabber_state"` topic. The service returns `True` if the grabber is closed and `False` if it is open. If the grabber is in a inbetween state (it's moving) when the service is called the server waits until it reaches a defined state (open or closed). If no defined state is reached after 10 secounds the garabber is asumed stuck and the server throws an Exception.

## Example
The package has a node called sample_client.py that provides an example on how the service can be called.

        $ rosrun mecanum_grabber sample_client.py

Or use the terminal command.

        $ rosservice call /control_grabber True
