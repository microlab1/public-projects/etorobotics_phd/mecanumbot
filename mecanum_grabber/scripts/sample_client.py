#!/usr/bin/env python
import rospy
from mecanum_grabber.srv import ControlGrabber
from mecanum_grabber.srv import GetGrabberState

def main():
    rospy.init_node("grabber_sample_client")
    rospy.wait_for_service("control_grabber")
    rospy.wait_for_service("get_grabber_state")

    control_client = rospy.ServiceProxy("control_grabber", ControlGrabber)
    success =  control_client(True)
    print(success)

    read_client = rospy.ServiceProxy("get_grabber_state", GetGrabberState)
    status = read_client()
    print(status)

if __name__ == "__main__":
    main()
