#include <ros/ros.h>
#include <mecanum_grabber/ControlGrabber.h>
#include <mecanum_grabber/GetGrabberState.h>
#include <geometry_msgs/Vector3.h>


class ControlGrabber{
public:
    ControlGrabber()
    {
        stuck = false;
        if(!n.getParam("/left_close", left_close))
        {
            ROS_ERROR_STREAM("Can't get param: /left_close from parameter server. Shutting down.");
            ros::shutdown();
            return;
        }
        if(!n.getParam("/left_open", left_open))
        {
            ROS_ERROR_STREAM("Can't get param: /left_open from parameter server. Shutting down.");
            ros::shutdown();
            return;
        }
        timer = n.createTimer(ros::Duration(10), &ControlGrabber::timerCallback, this, true, false);
        right_close = left_open;
        right_open = left_close;
        pub = n.advertise<geometry_msgs::Vector3>("cmd_grabber", 1);
        sub = n.subscribe("rob_grabber", 1, &ControlGrabber::subCallback, this);
        control_service = n.advertiseService("control_grabber", &ControlGrabber::controlServiceCallback, this);
        state_service = n.advertiseService("get_grabber_state", &ControlGrabber::getStateCallback, this);

        geometry_msgs::Vector3 msg;
        msg.z = 0;
        msg.x = left_open;
        msg.y = right_open;
        pub.publish(msg); //open grabber on startup to avoid fake stuck errors
    }

private:
    bool controlServiceCallback(mecanum_grabber::ControlGrabberRequest& req, mecanum_grabber::ControlGrabberResponse& res)
    {
        geometry_msgs::Vector3 msg;
        msg.z = 0;
        bool close = req.close;
        if(close) //close
        {
            msg.x = left_close;
            msg.y = right_close;
        }
        else //open
        {
            msg.x = left_open;
            msg.y = right_open;
        }
        pub.publish(msg);

        ros::Rate rate(10);

        while(closed != close)
        {
            if(stuck)
            {
                res.success = false;
                return true;
            }
            rate.sleep();
        }

        res.success = true;    
        return true;
    } //service cb

    bool getStateCallback(mecanum_grabber::GetGrabberStateRequest& req, mecanum_grabber::GetGrabberStateResponse& res)
    {
        ros::Rate rate(10);
        while(ros::ok())
        {
            if(!moving)
            {
                res.state = closed;
                return true;
            }
            else
            {
                rate.sleep();
            }
        }
        return false; //to avoid compiler warnings
    } //getStatecb

    void subCallback(const geometry_msgs::Vector3::ConstPtr& msg)
    {
        if(msg->x == left_close && msg->y == right_close)
        {
            timer.stop();
            closed = true;
            moving = false;
        }
        else
        {
            if(msg->x == left_open && msg->y == right_open)
            {
                timer.stop();
                closed = false;
                moving = false;
            }
            else
            {
                timer.setPeriod(ros::Duration(10), false);
                timer.start();
                moving = true;
            }
        }
    } //subCallback

    void timerCallback(const ros::TimerEvent&)
    {
        stuck = true;
        ROS_ERROR_STREAM("Grabber stuck!");
    } //timercb

    ros::NodeHandle n;
    ros::ServiceServer control_service;
    ros::ServiceServer state_service;
    ros::Publisher pub;
    ros::Subscriber sub;
    double left_close;
    double left_open;
    double right_open;
    double right_close;
    bool closed;
    bool moving;
    bool stuck;
    ros::Timer timer;
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "grabber_control");
    ros::NodeHandle n;

    ControlGrabber control_grabber;

    ROS_INFO("Mecanum grabber control ready.");
    ros::AsyncSpinner spinner(4);

    spinner.start();

    ros::waitForShutdown();
    return 0;
}