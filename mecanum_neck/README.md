# Mecanum Neck
Package to contorl the neck of the Mecanum robot usning RPY angles.
To use the package run the

        $ rosrun mecanum_neck control_neck

 command. To control the neck call the `ControlNeck` service on the `"control_neck"` topic.

The arguments are the desired pith and yaw angles in degrees (-90 to +90 in both cases).

The `sample_client.py` node provides an example on how to call the service.