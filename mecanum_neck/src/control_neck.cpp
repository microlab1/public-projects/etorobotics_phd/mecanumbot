#include <ros/ros.h>
#include <geometry_msgs/Vector3.h>
#include "mecanum_neck/ControlNeck.h"

class ContorlNeck{
public:
    ContorlNeck()
    {
        pub = n.advertise<geometry_msgs::Vector3>("cmd_neck", 1);
        service = n.advertiseService("control_neck", &ContorlNeck::serviceCallback, this);
    }

private:
    bool serviceCallback(mecanum_neck::ControlNeckRequest &req, mecanum_neck::ControlNeckResponse &res)
    {
        geometry_msgs::Vector3 msg;
        msg.z = 0.0;

        double pitch = req.pitch;
        double yaw = req.yaw;

        //check if the input is in the given bounds
        if(pitch > 90 || pitch < -90)
        {
            ROS_ERROR_STREAM("Unreachable pitch command");
            res.success = false;
            return true;
        }
        if(yaw > 90 || yaw < -90)
        {
            ROS_ERROR_STREAM("Unreachable yaw command");
            res.success = false;
            return true;
        }

        //convert angles to motor steps
        msg.y = pitch * p + 2048;
        msg.x = yaw * p + 2048;

        pub.publish(msg); //send command to motors

        res.success = true;
        return true;
    }

    const double p = 1024 / 90;
    ros::NodeHandle n;
    ros::Publisher pub;
    ros::ServiceServer service;
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "contorl_neck");

    ContorlNeck control_neck;

    ROS_INFO_STREAM("control_neck service ready");
    
    ros::spin();
    ros::shutdown();
    return 0;
}