#!/usr/bin/env python
import rospy
from mecanum_neck.srv import ControlNeck

def main():
    rospy.init_node("neck_sample_client")
    rospy.wait_for_service("control_neck")
    
    client = rospy.ServiceProxy("control_neck", ControlNeck)
    client(0, 0)


if __name__ == "__main__":
    main()